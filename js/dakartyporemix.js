/**
 * @file Dakar Typo Remix
 * @author Laurent Malys (http://www.laurent-malys.fr)
 * @version 0.1.0
 * @copyright (c) Laurent Malys, 2018 
 * 
 * MIT Licence
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 * 
 * @credits Dakar Typo Remix is build on top of paper.js and opentype.js 
 * 
 * Paper.js v0.11.5 - The Swiss Army Knife of Vector Graphics Scripting.
 * http://paperjs.org/
 * (c) 2011 - 2016, Juerg Lehni & Jonathan Puckey
 * http://scratchdisk.com/ & http://jonathanpuckey.com/
 * 
 * https://opentype.js.org v0.7.3
 * (c) Frederik De Bleser and other contributors
 * MIT License
 * Uses tiny-inflate by Devon Govett
 */

paper.install(window);
var vm;
var l_p, 
    current_font, 
    svg_item,
    fontsize = 100;

var dir = new Point(0,0);
var p, p_dec, p_contour, p_orig;
var p_contour_t;

l_p = [];

function debug_lp(path_list){
    /**
     * create debug view of path contained by path_list
     * draw the first two segments of exterior and interior path 
     *
     * @param {Array} path_list - a list of path 
     */
    
    path_list.forEach(function(it){
	var p = it.ext_path;
	pp = new Path({segments: [p.segments[0].point, p.segments[1].point]});
	pp.strokeColor = "#ff0000";
	
	pp2 = new Path({segments: [p.segments[1].point, p.segments[2].point]});
	pp2.strokeColor = "#00ff00";
	
	var ii = it.in_path[0];
	if (ii){
	    pp = new Path({segments: [ii.segments[0].point, ii.segments[1].point]});
	    pp.strokeColor = "#ff0000";
	    
	    pp2 = new Path({segments: [ii.segments[1].point, ii.segments[2].point]});
	    pp2.strokeColor = "#00ff00";
	}
    });     
}

function import_path(svg_file){
    /**
     * import a set of path from a svg file
     * return an array where each path is an object containing the raw path
     * the external path and a list of interior path
     *
     * @param {Array} svg_file - svg file as returned by paperjs importSVG
     */
    var l_p = [];
    svg_file.children.forEach(function(it){
	var p = new CompoundPath(it.pathData);
	var e = p.children[0];
	if (e.area < 0){
	    e.reverse();
	}
	var l_i = p.children.slice(1, p.children.length);
	l_i.forEach(function(d){
	    if (d.area > 0){
		d.reverse();
	    }
	});
	l_p.push({'path':p,
		  'ext_path':e,
		  'in_path':p.children.slice(1, p.children.length)
		 });
    });
    
    return l_p;
}

function simple_extrude(p, dir){
    /**
     * extrude the first curve of a path (used for debug)
     *
     * @param {Object} p - a paperjs Path
     * @param {Object} dir - extrusion direction (paperjs Point)
     */
    
    c = p.curves[0];
    var pp = new Path();
    var s0 = c.segment1.clone();
    var s0_0 = s0.clone();
    s0_0.handleIn = new Point(0,0);
    
    var s1 = c.segment2.clone();
    var s1_0 = s1.clone();
    s1_0.handleOut = new Point(0,0);
    
    //	     pp.selected = true;
    pp.addSegment(s0_0);
    pp.addSegment(s1_0);
    
    var m = new Matrix();
    m.translate(dir);
    
    var s11 = s1.clone();
    s11.transform(m);
    
    var s0_1 = s11.clone();
    s0_1.handleIn = new Point(0,0);
    s0_1.handleOut = s11.handleIn;//new Point(0,0);
    pp.addSegment(s0_1);
    
    
    var s00 = s0.clone();
    s00.transform(m);
    
    var s1_1 = s00.clone();
    s1_1.handleOut = new Point(0,0);
    s1_1.handleIn = s00.handleOut;
    pp.addSegment(s1_1);
    
    pp.closePath();
    
    return pp;
}

function extrude_curve(c, m, m2){
    
    var pp = new Path();
    var s0 = c.segment1.clone();
    var s1 = c.segment2.clone();
    
    s0.handleIn.set(0,0);
    s1.handleOut.set(0,0);
    
    var s2 = c.segment2.clone();
    //	     if (m2){
    //		 s2.transform(m2);
    //	     }else{
    s2.transform(m);
    //	     }
    s2.handleOut.set(s2.handleIn.x, s2.handleIn.y);
    s2.handleIn.set(0,0);
    
    var s3 = c.segment1.clone();
    s3.transform(m);
    s3.handleIn.set(s3.handleOut.x, s3.handleOut.y);
    s3.handleOut.set(0,0);
    
    pp.addSegment(s0);
    pp.addSegment(s1);
    pp.addSegment(s2);
    pp.addSegment(s3);
    pp.closePath();
    
    return pp
}

function extrude(p, dir, bot, up){
    /**
     * extrude a paperjs Path -p- with -dir- direction
     *
     * @param {Object} p - a paperjs Path
     * @param {Object} dir - extrusion direction (paperjs Point) 
     */
    
    var l_pp = [];
    
    var m = new Matrix();
    m.translate(dir);
    
    var curve_list=[];/* 
			 var select = true;
			 var val_select = 0.2;*/
    if (bot){
	var val_select = vm.bottomExtrudeRatio/100;
	console.log(vm.bottomExtrudeRatio);
	var r = new Path.Rectangle(p.bounds.x, 
				   p.bounds.y+(1-val_select)*p.bounds.height,
				   p.bounds.width,
				   p.bounds.height*val_select);
	/* 
	 * 		 r.selected = true;
	 * 		 project.layers[2].addChild(r);*/
	var cut_r = r.intersect(p);
	
	curve_list = cut_r.curves;
	
    }else{
	curve_list = p.curves;
    }
    
    if (up){
	var m_up = new Matrix();
	var dir_up = dir.clone();
	
	dir_up.y = dir_up.y * vm.upExtrudeRatio/100;
	//dir_up = dir_up.multiply(vm.upExtrudeRatio/100);
	m_up.translate(dir_up);
	console.log(dir_up);
	var val_select = vm.bottomExtrudeRatio/100;
	var r = new Path.Rectangle(p.bounds.x, 
				   p.bounds.y,
				   p.bounds.width,
				   p.bounds.height*(1-val_select));
	var cut_r = r.intersect(p);
	
	cut_r.curves.forEach(function(c){
	    
	    var pp = extrude_curve(c, m_up);
	    l_pp.push(pp);		     
	})
	
    }
    
    
    
    curve_list.forEach(function(c){
	var pp = extrude_curve(c, m);
	l_pp.push(pp);
    });
    
    var do_unite = true;
    var p_unite = p.clone()
    if (do_unite){
	l_pp.forEach(function(p){
	    p_unite = p_unite.unite(p)
	})
	
	project.layers[0].removeChildren();
	return p_unite;
    }else{
	return l_pp;
    }   
}

function double_extrude(p, direction, bottom, up){
    /**
     * double extrude a paperjs Path -p- with -direction- direction
     * extrude first horizontaly 
     * then extrude the resulted shape verticaly
     *
     * @param {Object} p - a paperjs Path
     * @param {Object} direction - extrusion direction (paperjs Point) 
     */
    
    console.log(direction);
    var p_extrude_x = extrude(p, new Point(direction.x, 0.01), bottom, up);
    var pp = extrude(p_extrude_x, new Point(0.01, direction.y), bottom, up);
    
    return pp;
}


function updateText(){
    l_p = [];
    console.log(fontsize);
    console.log(fontsize+100);
    var a = current_font.getPath(vm.input_text, 100, fontsize, fontsize);
    var p = new CompoundPath(a.toSVG());
    
    var m = new Matrix();
    //		     m.translate(100, 400);
    //		     m.scale(0.5, -0.5);
    
    p.transform(m);
    var pp = {
	'path': p,
	'ext_path':p.children[p.children.length],
	'in_path':p.children.slice(0, p.children.length-1)
    }
    l_p.push(pp);
    
    project.layers[4].removeChildren();
    
    p_orig = l_p[0].path.clone();
    p_orig.fillColor = "#000000";
    project.layers[4].addChild(p_orig);
    
    update();
    updateOffset();
}

function update(){
    var p_t;
    dir.set(vm.h, vm.v);
    
    
    if (vm.contour != 0){
	p_contour_t = OffsetUtils.offsetPath(l_p[0].path.clone(), -vm.contour);
    }else{
	p_contour_t = l_p[0].path.clone();
    }
    p_contour.setPathData(p_contour_t.pathData);
    
    if (vm.double_extrusion){
	p_t = double_extrude(p_contour_t, dir, vm.bottomExtrude, vm.upExtrude);
    } else {
	p_t = extrude(p_contour_t, dir, vm.bottomExtrude, vm.upExtrude);
    }
    
    p.setPathData(p_t.pathData);
    updateOffset();
    project.layers[0].visible = false;
    // var p_offset = OffsetUtils.offsetPath(l_p[0].path.clone(), 0);
}

function updateOffset(){
    if (vm.do_offset){
	project.layers[2].setVisible(true);
	var dir_dec;
	if (vm.double_extrusion){
	    
	    var min_r = Math.min(Math.abs(dir.x), Math.abs(dir.y));
	    var val_offset = min_r * vm.offset_length / 100;
	    var sens_x = 1;
	    var sens_y = 1;
	    
	    if (dir.x == 0){
		sens_x = 0;
	    }else{
		sens_x = dir.x/Math.abs(dir.x);
	    }
	    
	    if (dir.y == 0){
		sens_y = 0;
	    }else{
		sens_y = dir.y/Math.abs(dir.y);
	    }
	    
	    dir_dec = new Point(val_offset*sens_x,
				val_offset*sens_y);
	}else{
	    dir_dec = dir.clone().normalize().multiply(vm.offset_length*dir.length/100);
	}
	var p_dec_t;
	if (vm.double_extrusion){
	    p_dec_t = double_extrude(p_contour_t, dir_dec, false, false);
	}else{
	    p_dec_t = extrude(p_contour_t, dir_dec, false, false);
	}
	p_dec.setPathData(p_dec_t.pathData);
    }else{
	project.layers[2].setVisible(false);
    }
    
}

function updateFont(file_reader){
    current_font = opentype.parse(file_reader.result);
    updateText();
}

window.onload = function() {
    paper.setup('canvas');
    
    p = new CompoundPath();
    p.fillColor = "red";
    
    p_dec = new CompoundPath();
    p_dec.fillColor = "white";
    
    p_contour_t = new CompoundPath();
    p_contour = new CompoundPath();
    p_contour.fillColor = "yellow";
    
    p_orig = new CompoundPath();
    p_orig.fillColor = "black";
    
    
    var from_svg = false;
    var from_opentype = true;
    
    project.addLayer( new Layer() );
    project.addLayer( new Layer() );
    project.addLayer( new Layer() );
    project.addLayer( new Layer() );
    project.addLayer( new Layer() );
    
    project.layers[0].activate();
    
    project.layers[1].addChild(p);
    project.layers[2].addChild(p_dec);
    project.layers[3].addChild(p_contour);
    project.layers[4].addChild(p_orig);
    
    if (from_svg){
	project.importSVG('A.svg', function(item){
	    svg_item = item;
	    a = item.children[1];
	    console.log(item);
	    l_p = import_path(a);
	    
	    p_orig = l_p[0].path.clone();
	    p_orig.fillColor = "#ff0000";
	    project.layers[4].addChild(p_orig);
	    
	    update();
	});
    }
    
    if (from_opentype){
	var font_name = 'Montserrat-Black.otf'; 
	opentype.load(font_name, function(err, font){
	    if (err) {
		showErrorMessage(err.toString());
		return;
	    }
	    current_font = font;
	    updateText();
	});
    }
    
    vm = new Vue({
	el:'#app',
	data:{
	    fontsize: 100,
	    input_text: "B",
	    h: -50, 
	    v: 20,
	    contour:0,
	    double_extrusion: false,
	    do_offset: false,
	    offset_length: 5,
	    bottomExtrude: false,
	    bottomExtrudeRatio: 20,
	    upExtrude: false,
	    upExtrudeRatio: 20
	},
	methods:{
	    changeFont: function(){ 
		var selectedFile = document.getElementById('fileInput').files[0];
		var fr = new FileReader();
		fr.onload = function(event){
		    console.log("file loaded");
		    updateFont(fr)
		}
		fr.readAsArrayBuffer(selectedFile); 
	    },
	    updateText: updateText,
	    changeFontSize: function(){
		fontsize = this.fontsize;
		console.log(fontsize);
		updateText();
	    },
	    updateAll: function(){
		update();
		updateOffset();
	    },
	    updateDoDouble: function(){
		this.double_extrusion = !this.double_extrusion;
		update();
	    },
	    updateDoOffset: function(){
		this.do_offset = !this.do_offset;
		updateOffset();
	    },
	    updateOffset: updateOffset,
	    updateDoBottomExtrude: function(){
		this.bottomExtrude = !this.bottomExtrude;
		update();
	    },
	    updateDoUpExtrude: function(){
		this.upExtrude = !this.upExtrude;
		update();
	    },
	    exportSvg: function(){
		let a = project.exportSVG().outerHTML;
		console.log('toto');
		$('<a></a>')
		    .attr('id','downloadFile')
		    .attr('href','data:text/xml;charset=utf8,' + encodeURIComponent(a))
		    .attr('download','dakarTypoRemix.svg')
		    .text('dd')
		    .appendTo('body');
		
		$('#downloadFile').get(0).click();
		$('#downloadFile').remove();
	    }	    
	}
    })    
}
